// A terrible & messy framework for writing IRCD Service servers.
// From statistics to chat bots to relays. Anything you wish!
// This framework is written to link with UnrealIRCD 4 and may not work on any versions below UnrealIRCD 4!

const net = require('net');
const EventEmitter = require('events');
const vm = require('vm');
const fs = require('fs');
//const uuidV4 = require('uuid/v4');
const crypto = require('crypto'); // Replace with UUID When it wants to work with winblows :c
const crypkey = (Math.random()*10000000000000000).toString(16); // Not for security, just randomness

// Our own libs
const Bot = require('./lib/Bot.js');
const Channel = require('./lib/Channel.js');
const Server = require('./lib/Server.js');
const User = require('./lib/User.js');

class IRCD {
	constructor() {
		// Default values
		this.version = "0.0.2";
		this.software = "NodeIRCD";
		this.address = "node.mynet.org";
		this.desc = "A NodeJS IRCD Thing";
		
		// Config
		this.config = {};
		this.unreal4 = true; /* Are we connecting to U4 or U3? */
		
		// State related.
		this.connecting = false;
		this.socket = null;
		
		// Stuff we know
		this.users = [];
		this.channels = [];
		this.servers = [];
		this.bots = []; /* Our own bots */
		
		// Events
		this.events = new EventEmitter();
	}
	
	connect(dest) {
		if (this.connecting || this.socket != null) return;
		this.connecting = true;
		
		this.socket = net.createConnection(dest.port,dest.host, () => {
			
			// Create connection.
			this.send(`PASS :${dest.pass}`);
			//this.send(`PROTOCTL EAUTH=${this.address} SID=159`);
			this.send(`PROTOCTL NOQUIT NICKv2 SJOIN SJ3 CLK TKLEXT TKLEXT2 NICKIP ESVID MLOCK`);
			// Removed EXTSWHOIS - idk the syntax
			this.send(`SERVER ${this.address} 2 :${this.desc}`);
			
			// Add our own server.
			let server = new Server();
			server.name = this.address;
			server.desc = this.desc;
			server.hopcount = 0;
			this.servers.push(server);
		});
		
		this.socket.on('data', data => {
			data = data.toString().split("\n");
			data.forEach( line => {
				line = line.trim();
				if (line == "") return;
				console.log(" [<-] "+line);
				
				const parsed = this.parse(line);
				
				if (parsed.command == "PING") {
					this.send(`PONG :${parsed.args[0]}`);
				} else if (parsed.command == "EOS") {
					if (parsed.server.toLowerCase() == dest.name.toLowerCase()) {
						this.events.emit("ready");
						this.send("EOS");
						this.connectBots();
					}
				}
				//console.log(parsed);
				this.events.emit(parsed.command.toLowerCase(),parsed);
			});
		});
		
		this.socket.on('end', () => {
			this.connecting = false;
			this.socket = false;
		});
		
	}
	send(line) {
		if (!this.connecting || this.socket == null) return;
		console.log(" [->] "+line);
		this.socket.write(`${line}\n`);
	}

	parse(line) {
		var message = {};
		var match;

		// Parse prefix
		match = line.match(/^:([^ ]+) +/);
		if (match) {
			message.prefix = match[1];
			line = line.replace(/^:[^ ]+ +/, '');
			match = message.prefix.match(/^([_a-zA-Z0-9\~\[\]\\`^{}|-]*)(!([^@]+)@(.*))?$/);
			if (match) {
				message.nick = match[1];
				message.user = match[3];
				message.host = match[4];
			}
			else {
				message.server = message.prefix;
			}
		}

		// Parse command
		match = line.match(/^([^ ]+) */);
		message.command = match[1];
		message.rawCommand = match[1];
		line = line.replace(/^[^ ]+ +/, '');

		message.args = [];
		var middle, trailing;

		// Parse parameters
		if (line.search(/^:|\s+:/) != -1) {
			match = line.match(/(.*?)(?:^:|\s+:)(.*)/);
			middle = match[1].trimRight();
			trailing = match[2];
		}
		else {
			middle = line;
		}

		if (middle.length)
			message.args = middle.split(/ +/);

		if (typeof (trailing) != 'undefined' && trailing.length)
			message.args.push(trailing);

		return message;
	}
	
	loadBots() {
		console.log(" = Loading bots =");
		return new Promise((resolve,reject)=>{
			fs.readdir("./bots",(err,files)=>{
				if (err) {
					reject(err);
				} else {
					resolve(files);
				}
			});
		}).then((files)=>{
			return Promise.all(files.map((file) => {
				try {
					const env = {
						API:this,
						Bot:Bot,
						require:require,
						process:process,
						console:console,
						setInterval:setInterval,
						setTimeout:setTimeout,
						clearInterval:clearInterval,
						clearTimeout:clearTimeout,
						parseInt:parseInt,
						decodeURI:decodeURI,
						decodeURIComponent:decodeURIComponent,
						encodeURI:encodeURI,
						encodeURIComponent:encodeURIComponent,
						isFinite:isFinite,
						isNaN:isNaN,
						Number:Number,
						parseFloat:parseFloat,
						String:String,
						eval:eval,
						JSON:JSON,
					}
					const context = vm.createContext(env);
					const script = new vm.Script(fs.readFileSync("./bots/"+file).toString(), {filename:__dirname+"/bots/"+file});
					const ret = script.runInContext(context);
					
					console.log("Loaded script '%s'",file);
					
				} catch (e) {
					console.log(e);
					return false;
				}
				return true;
			})).then(()=>{
				console.log(" - Finished loading scripts -");
			});
		});
	}
	connectBots() {
		console.log("Connecting "+this.bots.length+" bots..");
		this.bots.forEach( bot => {
			bot.connect();
		});
	}
	registerBot(bot) {
		bot.ircd = this;
		this.bots.push(bot);
		return true;
	}
	shutdown() {
		this.bots.forEach( bot => {
			bot.disconnect("Shutdown");
		});
	}
	
	getBot(nick){
		return null;
	}
	getUser(nick) {
		for (let u in this.users) {
			if (this.users[u].nick.toLowerCase() == nick.toLowerCase()) {
				return this.users[u];
			}
		}
		return null;
	}
	getChannel(name) {
		for (let c in this.channels) {
			if (this.channels[c].name.toLowerCase() == name.toLowerCase()) {
				return this.channels[c];
			}
		}
		return null;
	}
	getServer(name) {
		for (let s in this.servers) {
			if (this.servers[s].name.toLowerCase() == name.toLowerCase()) {
				return this.servers[s];
			}
		}
		return null;
	}
}

// Create IRCD
let ircd = new IRCD();

// Events.
ircd.events.on('ping',() => {
	console.log("I GOT A PING WOO");
});
ircd.events.on('ready',() => {
	console.log("Sync'd");
});
ircd.events.on('nick', ev => {
	// Nick change or user register!
	
	let who = ev.args[0];
	if (typeof ev.nick != "undefined") who = ev.nick;
	
	let newnick = ev.args[0];
	let user = ircd.getUser(who);
	
	if (user == null) {
		user = new User(ircd);
		ircd.users.push(user);
		// also a user connect!
	}
	
	// Update the user!
	user.nick = newnick;
	
	//console.log(ev);
	
	// Get some reliable basics.
	if (ev.args.length > 2) {
		user.hopcount = ev.args[1];
		user.connectStamp = ev.args[2];
		
		if (ev.args.length > 3) {
			try {
				user.username = ev.args[3];
				user.hostname = ev.args[4];
				user.server = ev.args[5];
				user.services = ev.args[6];
				user.modes = ev.args[7].substr(1).split("");
				user.vhost = ev.args[8];
				user.cloaked = ev.args[9];
				user.ip = ev.args[10];
				user.realname = ev.args[11];
			} catch (e) {
				console.log(ev);
				throw e;
			}
		}
	}
});
ircd.events.on('away', ev => {
	console.log(ev);
	let user = ircd.getUser(ev.nick);
	if (user != null) {
		if (ev.args.length > 0) {
			user.away.status = true;
			user.away.reason = ev.args[0];
			user.away.stamp = Math.floor(Date.now()/1000);
		} else {
			user.away.status = false;
			user.away.reason = "";
		}
	}

});
ircd.events.on('part', ev => {
	console.log(ev);
	
	let user = ircd.getUser(ev.nick);
	let chan = ircd.getChannel(ev.args[0]);
	
	if (chan != null) {
		for (let u in chan.users) {
			if (chan.users[u].user == user) {
				chan.users.splice(u,1);
			}
		}
		if (chan.users.length <= 0) {
			console.log(`${chan.name} is now empty, destroying.`);
			ircd.channels.splice(ircd.channels.indexOf(chan),1);
		}
	}
});
ircd.events.on('quit', ev => {
	
	let nick = ev.nick;
	let reason = "";
	if (ev.args.length > 0) reason = ev.args[0];
	
	let user = ircd.getUser(nick);
	if (user != null) {
		
		// Remove them from channels.
		ircd.channels.forEach( ch => {
			ch.users.forEach( u => {
				if (u.user == user) {
					ch.users.splice(ch.users.indexOf(u),1);
					
					if (ch.users.length <= 0) {
						console.log(`${ch.name} is now empty, destroying.`);
						ircd.channels.splice(ircd.channels.indexOf(ch),1);
					}
					
				}
			});
		});
		
		// Remove them from the ircd
		ircd.users.splice(ircd.users.indexOf(user),1);
	}
});
ircd.events.on('swhois', ev => {
	
	let nick = ev.args[0];
	let message = ev.args[1];
	
	let user = ircd.getUser(nick);
	
	if (user != null) {
		user.swhois = message;
	}
});
ircd.events.on('sjoin', ev => {
	let channel = ircd.getChannel(ev.args[1]);
	
	if (channel == null) {
		channel = new Channel(ircd);
		channel.name = ev.args[1];
		ircd.channels.push(channel);
	}
	
	channel.stamp = ev.args[1];
	
	let users;
	if (ev.args.length == 4) {
		// Modes present.
		channel.modes = ev.args[2].substr(1).split("");
		users = ev.args[3].split(" ");
	} else {
		// No modes.
		users = ev.args[2].split(" ");
	}
	
	// Mode mapping
	let map = {};
	map['*'] = "q";
	map['@'] = "o";
	map[''] = "";
	
	users.forEach( u => {
		let matches = u.match(/([@*~&]*)([A-z0-9]+)/i);
		if (matches) {
			let nick = matches[2];
			let modes = matches[1];
			
			let user = ircd.getUser(nick);
			if (user != null) {
				channel.users.push({ user:user, modes:modes.split("") });
				user.channels.push(channel);
			}
		}
	});
	/*
	 *  	{ prefix: 'Hypnos.CakeForce.co.uk',
	 * server: 'Hypnos.CakeForce.co.uk',
	 * command: 'SJOIN',
	 * rawCommand: 'SJOIN',
	 * args:
	 * 0 [ '1487207315',
	 * 1	 '#staff',
	 * 2	 '+ntrO',
	 * 3	 '@~CakeForce @~CakeBot @~BOPM @~ProxyScan @~Fudgie' ] }
	*/
});
ircd.events.on('topic', ev => {
	
	let channel = ircd.getChannel(ev.args[0]);
	if (channel == null) {
		channel = new Channel(ircd);
		channel.name = ev.args[0];
		ircd.channels.push(channel);
	}
	
	let user = ircd.getUser(ev.args[1]);
	if (user == null) return;
	
	channel.topic = { topic: ev.args[3], setter: user, stamp: ev.args[2] };
	
	/*
		[<-] TOPIC #staff CakeBot 1491053372 :Staff Only Room |=| |=| http://cakeforce.uk
		{ command: 'TOPIC',
		  rawCommand: 'TOPIC',
		  args:
		   [ 0'#staff',
			 1'CakeBot',
			 2'1491053372',
			 3'Staff Only Room |=| |=| http://cakeforce.uk' ] }
	*/
});
ircd.events.on('server', ev => {
	
	let server = ircd.getServer(ev.args[0]);
	if (server == null) {
		server = new Server();
	}
	
	server.name = ev.args[0];
	server.hopcount = ev.args[1];
	server.desc = ev.args[2];
	
	/*
	 [<-] :Zeus.CakeForce.co.uk SERVER Zulu.CakeForce.co.uk 3 :Zulu - CakeForceUK
	{ prefix: 'Zeus.CakeForce.co.uk',
	  server: 'Zeus.CakeForce.co.uk',
	  command: 'SERVER',
	  rawCommand: 'SERVER',
	  args: [ 'Zulu.CakeForce.co.uk', '3', 'Zulu - CakeForceUK' ] }
	*/
});
ircd.events.on('privmsg', ev => {
	console.log("le mssgggg",ev);
	
	let channel = ev.args[0];
	let message = ev.args[1];
	
	if (!message || message == "") return;
	
	let ex = message.split(" ");
	
	let ctcp = false;
	
	if (message.charCodeAt(0) == 1 && message.charCodeAt(message.length-1) == 1) {
		// May be a CTCP!
		ctcp = ex[0].substr(1);
		if (ctcp.charCodeAt(ctcp.length-1) == 1) ctcp = ctcp.substr(0,ctcp.length-1);
		
		message = message.substr(1+ctcp.length);
		message = message.substr(0,message.length-1).trim();
	}
	
	// Generate an ID for the message
	let id = crypto.createHmac('sha1', crypkey).update( Date.now() + message + channel + ev.nick ).digest('hex');
	let payload = {
		id : id,
		channel : channel,
		message : message,
		nick : ev.nick
	};
	ircd.bots.forEach( bot => {
		if (!ctcp) {
			//console.log("triggering message",ctcp);
			bot.events.emit('message',payload);
		} else {
			if (ctcp.toLowerCase() == "action") {
				bot.events.emit('action',payload);
			} else {
				payload.ctcp = ctcp;
				bot.events.emit('ctcp',payload);
			}
		}
	});
});

// Make the connection! ~ Ideally this belongs within the main class
fs.readFile('data/config.json', function(err, data){
	if (err) throw err;
	const config = JSON.parse(data);
	ircd.config = config;
	ircd.address = config.self.address;
	ircd.desc = config.self.description;
	ircd.unreal4 = config.self.unreal4;
	ircd.loadBots();
	ircd.connect(config.dest);
});

process.on('SIGINT', function () {
	console.log("Attempting graceful shutdown!");
	ircd.shutdown();
	process.exit(0);
});
const EventEmitter = require('events');

class Bot {
	constructor(ircd) {
		this.ircd = null;
		if (typeof ircd != "undefined") this.ircd = ircd;
		
		this.nick = "Bot";
		this.realname = "A bot!";
		this.username = "bot";
		this.hostname = "bot.local";
		this.modes = ["i","w","x"];
		this.cloakedhost = "bot.local";
		this.uid = Date.now();
		
		// My own events!
		this.events = new EventEmitter();
		
		// State
		this.channels = [];
	}
	connect() {
		let user = this.ircd.getUser(this.nick);
		console.log(user);
		if (user != null) {
			this.ircd.send(`SKILL ${user.nick} :Nick Collision!`);
		}
		let stamp = Math.floor(Date.now() / 1000);
		//              UID  nickname hopcount timestamp username hostname uid servicestamp umodes virthost cloakedhost ip :gecos
		
		// Unreal 4?
		if (this.ircd.unreal4) {
			// Unreal 4
			this.ircd.send(`UID ${this.nick} 1 ${stamp} ${this.username} ${this.hostname} ${this.uid} +${this.modes.join("")} * ${this.hostname} * :${this.realname}`);
		} else {
			// Unreal 3 (UNTESTED)
			//              NICK nickname hopcount timestamp username hostname server servicestamp usermodes virtualhost cloakedhost ip :realname
			this.ircd.send(`NICK ${this.nick} 1 ${stamp} ${this.username} ${this.hostname} ${this.ircd.address} ${this.nick} +${this.modes.join("")} ${this.hostname} * :${this.realname}`);
		}
		this.events.emit("connect");
	}
	disconnect(reason) {
		if (reason) {
			this.ircd.send(`:${this.nick} QUIT :${reason}`);
		} else {
			this.ircd.send(`:${this.nick} QUIT`);
		}
	}
	join(channel) {
		this.channels.push(channel);
		this.ircd.send(`:${this.nick} JOIN ${channel}`);
		this.events.emit('join',{nick:this.nick,channel:this.channel});
	}
	say(channel,message) {
		this.ircd.send(`:${this.nick} PRIVMSG ${channel} :${message}`);
	}
	act(channel, message) {
		this.ircd.send(`:${this.nick} PRIVMSG ${channel} :${String.fromCharCode(1)}ACTION ${message}${String.fromCharCode(1)}`);
	}
	send(raw) {
		this.ircd.send(`:${this.nick} ${raw}`);
	}
	ctcp(dest,what,message) {
		if (typeof message != "undefined") message = ` ${message}`;
		this.ircd.send(`:${this.nick} NOTICE ${dest} :${String.fromCharCode(1)}${what.toUpperCase()} ${message}${String.fromCharCode(1)}`);
	}
}
module.exports = Bot;
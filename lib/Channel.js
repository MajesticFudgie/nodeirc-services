class Channel {
	constructor() {
		this.name = "#";
		this.stamp = 0; /* Creation Stamp */
		this.topic = null;
		this.modes = [];
		this.users = [];
	}
}
module.exports = Channel;
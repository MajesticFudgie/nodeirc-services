// A normal IRC user
class User {
	constructor(ircd) {
		this.ircd = null;
		if (typeof ircd != "undefined") this.ircd = ircd;
		
		this.nick = "";
		this.username = "";
		this.hostname = "";
		this.ip = "";
		this.cloaked = "";
		this.services = 0;
		this.hopcount = 0;
		this.realname = "";
		this.modes = [];
		this.channels = [];
		this.swhois = "";
		this.server = "";
		this.away = {status:false , reason:"", stamp:0};
		this.connectStamp = 0;
	}
}
module.exports = User;

